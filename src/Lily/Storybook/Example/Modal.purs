module Lily.Storybook.Example.Modal where

import Prelude
import Data.Maybe(Maybe(..))
import Data.Const (Const)
import Data.Foldable(for_)

import Effect.Aff.Class (class MonadAff)

import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Properties as HP
import Halogen.HTML.Events as HE
import Halogen.HTML.Core(ClassName(..))

import Lily.Part.Modal (wrap, initializeWith, close, defaultConfig)
import Lily.Block.Conditional (conditional_)
import Lily.Block.Button as BTN

type Query = Const Void

type State =
  { enabled :: Boolean
  , modalSid :: Maybe H.SubscriptionId
  }

data Action = Toggle
  | OnClose



initialState :: State
initialState = { enabled: false, modalSid: Nothing }



render :: forall m. State -> H.ComponentHTML Action () m
render state =
  HH.div_
    [ BTN.buttonPrimary
        [ HE.onClick \_ -> Just Toggle ]
        [ HH.text "Toggle Modal" ]
      , conditional_
          state.enabled
          [ (wrap demoModalConfig OnClose [] [ HH.text "Modal content goes here" ]) ]
    ]


demoModalConfig =
  defaultConfig {
    title = Just "Modal Title"
  }



component :: forall q i o m. MonadAff m => H.Component HH.HTML q i o m
component = H.mkComponent
  { initialState: const initialState
  , render
  , eval: H.mkEval $ H.defaultEval { handleAction = handleAction }
  }



handleAction ∷ forall o m. MonadAff m => Action -> H.HalogenM State Action () o m Unit
handleAction = case _ of
  Toggle -> do
    modalSid <- initializeWith OnClose
    H.modify_ \st -> st { enabled = not st.enabled, modalSid = Just modalSid }

  OnClose -> do
    state <- H.get
    for_ (state.modalSid) \sid -> do
      close sid
    H.modify_ \st -> st { enabled = not st.enabled, modalSid = Nothing }
