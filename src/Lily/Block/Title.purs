module Lily.Block.Title where

import Prelude

import DOM.HTML.Indexed (HTMLh4)
import Halogen.HTML as HH
import Halogen.HTML.Properties as HP

import Lily.HTML.Properties ((<&>))



formTitleClasses :: Array HH.ClassName
formTitleClasses = HH.ClassName <$>
  [ "text-xl"
  , "md:text-2xl"
  , "text-center"
  , "text-blue-900"
  , "font-medium"
  , "leading-tight"
  , "mb-4"
  , "md:mb-5"
  , "md:mt-0"
  ]



titleBuilder
  :: forall p i
   . Array HH.ClassName
  -> Array (HH.IProp HTMLh4 i)
  -> Array (HH.HTML p i)
  -> HH.HTML p i
titleBuilder classes iprops =
  HH.h4
    ( [ HP.classes $ classes ] <&> iprops )



formTitle
  :: ∀ p i
   . Array (HH.IProp HTMLh4 i)
  -> Array (HH.HTML p i)
  -> HH.HTML p i
formTitle =
  titleBuilder formTitleClasses


formTitle_
  :: ∀ p i
   . Array (HH.HTML p i)
  -> HH.HTML p i
formTitle_ =
  formTitle []
