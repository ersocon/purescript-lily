{-
Welcome to a Spago project!
You can edit this file as you like.
-}
{ name = "purescript-lily"
, dependencies =
  [ "console"
  , "effect"
  , "halogen"
  , "halogen-storybook"
  , "halogen-hooks"
  , "psci-support"
  , "record"
  , "transformers"
  , "argonaut"
  ]
, packages = ./packages.dhall
, sources = [ "src/**/*.purs", "test/**/*.purs" ]
}
