let mkPackage =
      https://raw.githubusercontent.com/purescript/package-sets/psc-0.13.6-20191127/src/mkPackage.dhall sha256:0b197efa1d397ace6eb46b243ff2d73a3da5638d8d0ac8473e8e4a8fc528cf57

let upstream =
      https://github.com/purescript/package-sets/releases/download/psc-0.13.6-20200226/packages.dhall sha256:3a52562e05b31a7b51d12d5b228ccbe567c527781a88e9028ab42374ab55c0f1

let overrides = {=}

let additions =
      { datetime-iso =
          mkPackage
            [ "prelude", "argonaut-codecs", "parsing", "datetime", "newtype" ]
            "https://github.com/jmackie/purescript-datetime-iso.git"
            "v3.0.0"
      , halogen-select =
          mkPackage
            [ "halogen", "record" ]
            "https://github.com/citizennet/purescript-halogen-select.git"
            "v5.0.0-rc.4"
      , halogen-storybook =
          mkPackage
            [ "halogen", "routing", "foreign-object" ]
            "https://github.com/rnons/purescript-halogen-storybook.git"
            "v1.0.0-rc.1"
      , halogen-hooks =
          mkPackage 
            [ "halogen", "indexed-monad" ]
            "https://github.com/thomashoneyman/purescript-halogen-hooks.git"
            "v0.4.1"
      , halogen =
          mkPackage 
            [ "aff"
    , "avar"
    , "console"
    , "const"
    , "coroutines"
    , "dom-indexed"
    , "effect"
    , "foreign"
    , "fork"
    , "free"
    , "freeap"
    , "halogen-vdom"
    , "media-types"
    , "nullable"
    , "ordered-collections"
    , "parallel"
    , "profunctor"
    , "transformers"
    , "unsafe-coerce"
    , "unsafe-reference"
    , "web-file"
    , "web-uievents" ]
            "https://github.com/purescript-halogen/purescript-halogen.git"
            "v5.0.0"
      }

in  upstream // overrides // additions
