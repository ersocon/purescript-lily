module Lily.Part.Modal where

import Prelude

import Data.Maybe (Maybe(..), isJust, fromMaybe)
import Data.Foldable (for_)

import DOM.HTML.Indexed (HTMLdiv)

import Effect.Aff.Class (class MonadAff)

import Web.HTML (window) as Web
import Web.HTML.HTMLDocument as HTMLDocument
import Web.HTML.Window (document) as Web
import Web.HTML.HTMLElement (toElement)

import Web.DOM.ParentNode (QuerySelector(..))
import Web.DOM.Element (setAttribute, removeAttribute)

import Halogen as H
import Halogen.Aff.Util (selectElement)
import Halogen.HTML as HH
import Halogen.HTML.Core (ClassName(..))
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Halogen.Query.EventSource as ES

import Web.UIEvent.KeyboardEvent as KE
import Web.UIEvent.KeyboardEvent.EventTypes as KET

import Lily.Block.Conditional (conditional, altProps) as UI
import Lily.Block.Title (formTitle) as UI


type ModalConfig =
  { title :: Maybe String
  , outClickClose :: Boolean
  }


defaultConfig :: ModalConfig
defaultConfig =
  { title: Nothing
  , outClickClose: true
  }


modalClasses :: Array ClassName
modalClasses =
  ClassName <$> [
    "fixed"
  , "pin"
  , "bg-black-modal-a90"
  , "fade-in"
  , "z-40"
  ]


modalContentClasses :: Array ClassName
modalContentClasses =
  ClassName <$> [
    "fixed"
  , "pin-x"
  , "pin-t"
  , "my-20"
  , "m-auto"
  , "max-w-lg"
  , "slide-down"
  , "z-50"
  ]



modalTitleClasses :: Array ClassName
modalTitleClasses =
  ClassName <$> [
    "text-2xl"
  , "sm:text-xl"
  , "text-center"
  , "text-blue-900"
  , "font-semibold"
  , "leading-tight"
  , "font-heading"
  , "mb-4"
  , "sm:mb-3"
  , "sm:mt-1"
  ]



initializeWith
  :: forall s action childSlots o m. MonadAff m => action -> H.HalogenM s action childSlots o m H.SubscriptionId
initializeWith action = do

  maybeHtml <- H.liftAff $ selectElement (QuerySelector "html")
  for_ (maybeHtml) \element ->
    H.liftEffect $ setAttribute "data-modal-mode" "true" (toElement element)

  document <- H.liftEffect $ Web.document =<< Web.window

  H.subscribe
    $ ES.eventListenerEventSource
      KET.keyup
      (HTMLDocument.toEventTarget document)
      (handleKey <=< KE.fromEvent)
  where
    handleKey :: KE.KeyboardEvent -> Maybe action
    handleKey keyboardEvent = case (KE.key keyboardEvent) of
      "Escape" -> Just action
      _ -> Nothing



close
  :: forall s action childSlots o m. MonadAff m => H.SubscriptionId -> H.HalogenM s action childSlots o m Unit
close sid = do
  maybeHtml <- H.liftAff $ selectElement (QuerySelector "html")
  for_ (maybeHtml) \element ->
    H.liftEffect $ removeAttribute "data-modal-mode" (toElement element)

  H.unsubscribe sid
  pure unit




----------
-- Render Partials

-- Modals already come with the ClickOutside event baked in, while
-- end users are responsible for handling it somehow.
modal
  :: forall action childSlots m
   . action
  -> Array (HH.IProp HTMLdiv (action))
  -> Array (H.ComponentHTML action childSlots m)
  -> H.ComponentHTML action childSlots m
modal dismissModalAction iprops html =
  HH.div
    [ HP.classes $ ClassName <$> ["modal", "is-active"] ]
    [ HH.div
        [ HP.class_ $ ClassName "modal-background"
        , HE.onClick $ Just <<< const dismissModalAction
        ]
        []
    , HH.div
        [ HP.class_ $ ClassName "modal-content modal-content--slim" ]
        [ HH.div
            ( iprops )
            html
        ]
    , HH.button
        [ HP.classes $ ClassName <$> ["modal-close", "is-large"]
        , HE.onClick $ Just <<< const dismissModalAction
        ]
        []
    ]



----------
-- Render Partials

-- Modals already come with the ClickOutside event baked in, while
-- end users are responsible for handling it somehow.
wrap
  :: forall action childSlots m
  . ModalConfig
  -> action
  -> Array (HH.IProp HTMLdiv (action))
  -> Array (H.ComponentHTML action childSlots m)
  -> H.ComponentHTML action childSlots m
wrap modalConfig dismissModalAction iprops html =
  HH.div_
    [ HH.div
        (UI.altProps (modalConfig.outClickClose) [ HP.classes modalClasses, HE.onClick $ Just <<< const dismissModalAction ] [ HP.classes modalClasses ])
        []
    , HH.div
        [ HP.classes modalContentClasses ]
        [ UI.conditional (isJust modalConfig.title)
            []
            [ UI.formTitle
                []
                [ HH.text $ fromMaybe "" modalConfig.title ]
            ]
        , HH.div
            [ HP.classes $ ClassName <$> [ "bg-white", "rounded" ] ]
            [ HH.div
                ( iprops )
                html
            ]
        ]
    , HH.button
        [ HP.classes $ ClassName <$> ["modal-close", "is-large"]
        , HE.onClick $ Just <<< const dismissModalAction
        ]
        []
    ]
