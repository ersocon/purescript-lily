module Lily.Component.Renderless.Modal.Setters where

import Prelude (append)

import Data.Maybe (Maybe(..))
import Halogen as H
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP

import Web.UIEvent.FocusEvent as FE
import Web.UIEvent.KeyboardEvent as KE
import Web.UIEvent.MouseEvent as ME

import Lily.Component.Renderless.Modal

-- | The properties that must be supported by the HTML element that serves
-- | as a close toggle. This should be used with toggle-driven `Select` components.
type DismissProps props =
  ( onFocus :: FE.FocusEvent
  , onKeyDown :: KE.KeyboardEvent
  , onMouseDown :: ME.MouseEvent
  , onClick :: ME.MouseEvent
  , onBlur :: FE.FocusEvent
  , tabIndex :: Int
  | props
  )

-- | A helper function that augments an array of `IProps` with `ToggleProps`. It
-- | allows the toggle element to register key events for navigation or highlighting,
-- | record open and close events based on focus and blur, and to be focused with
-- | the tab key.
-- |
-- | ```purescript
-- | renderToggle = div (setDismissProps [ class "btn-class" ]) [ ...html ]
-- | ```
setDismissProps
  :: forall props act
   . Array (HP.IProp (DismissProps props) (Action act))
  -> Array (HP.IProp (DismissProps props) (Action act))
setDismissProps = append
  [ HE.onClick \_ -> Just DismissModal
  , HP.ref (H.RefLabel "modal-close")
  ]