module Lily.Component.Renderless.Modal where

import Prelude

import Data.Const (Const)
import Data.Maybe (Maybe(..))
import Data.Symbol (SProxy(..))
import Data.Traversable (for_)

import Control.Monad.Free (liftF)

import Effect.Aff.Class (class MonadAff)

import Record.Builder as Builder

import Prim.Row as Row

import Halogen as H
import Halogen.HTML as HH
import Halogen.Query.ChildQuery (ChildQueryBox)

import Lily.Part.Modal (initializeWith, close)


-----
-- ACTIONS

data Action action
  = Initialize (Maybe action)
  | Finalize (Maybe action)
  | DismissModal
  | OpenModal
  | Action action

type Action' = Action Void

-----
-- QUERIES

data Query query slots a
  = Send (ChildQueryBox slots (Maybe a))
  | Query (query a)

type Query' = Query (Const Void) ()


-----
-- Event

data Event = ModalDismissed


-----
-- HELPER TYPES

data ModalVisibility = Closed | Open
derive instance eqVisibility :: Eq ModalVisibility
derive instance ordVisibility :: Ord ModalVisibility

-- | The component slot type for easy use in a parent component
type Slot query slots msg = H.Slot (Query query slots) msg

-- | The component slot type when there is no extension
type Slot' = Slot (Const Void) () Void

-- | The component state
type State st =
  { visibility :: ModalVisibility
  , modalSubscriptionId :: Maybe H.SubscriptionId
  | st
  }

type Input st =
  { | st
  }


type Component query slots input msg m =
  H.Component HH.HTML (Query query slots) input msg m

type ComponentHTML action slots m =
  H.ComponentHTML (Action action) slots m

type HalogenM st action slots msg m a =
  H.HalogenM (State st) (Action action) slots msg m a


type Spec st query action slots input msg m =
  { -- usual Halogen component spec
    render
      :: State st
      -> ComponentHTML action slots m

    -- handle additional actions provided to the component
  , handleAction
      :: action
      -> HalogenM st action slots msg m Unit

    -- handle additional queries provided to the component
  , handleQuery
      :: forall a
       . query a
      -> HalogenM st action slots msg m (Maybe a)

    -- handle messages emitted by the component; provide H.raise to simply
    -- raise the Select messages to the parent.
  , handleEvent
      :: Event
      -> HalogenM st action slots msg m Unit

    -- optionally handle input on parent re-renders
  , receive
      :: input
      -> Maybe action

    -- perform some action when the component initializes.
  , initialize
      :: Maybe action

    -- optionally perform some action on initialization. disabled by default.
  , finalize
      :: Maybe action
  }

type Spec' st input m = Spec st (Const Void) Void () input Void m


defaultSpec
  :: forall st query action slots input msg m
   . Spec st query action slots input msg m
defaultSpec =
  { render: const (HH.text mempty)
  , handleAction: const (pure unit)
  , handleQuery: const (pure Nothing)
  , handleEvent: const (pure unit)
  , receive: const Nothing
  , initialize: Nothing
  , finalize: Nothing
  }


component
  :: forall st query action slots input msg m
   . MonadAff m
  => Row.Lacks "visibility" st
  => Row.Lacks "modalSubscriptionId" st
  => (input -> Input st)
  -> Spec st query action slots input msg m
  -> H.Component HH.HTML (Query query slots) input msg m
component mkInput spec = H.mkComponent
  { initialState: initialState <<< mkInput
  , render: spec.render
  , eval: H.mkEval
      { handleQuery: handleQuery spec.handleQuery
      , handleAction: handleAction spec.handleAction spec.handleEvent
      , initialize: Just (Initialize spec.initialize)
      , receive: map Action <<< spec.receive
      , finalize: Just (Initialize spec.finalize)
      }
  }
  where
  initialState :: Input st -> State st
  initialState = Builder.build pipeline
    where
    pipeline =
      Builder.insert (SProxy :: _ "visibility") Closed
        >>> Builder.insert (SProxy :: _ "modalSubscriptionId") Nothing


handleQuery
  :: forall st query action slots msg m a
   . MonadAff m
  => (query a -> HalogenM st action slots msg m (Maybe a))
  -> Query query slots a
  -> HalogenM st action slots msg m (Maybe a)
handleQuery handleQuery' = case _ of
  Send box ->
    H.HalogenM $ liftF $ H.ChildQuery box

  Query query ->
    handleQuery' query


handleAction
  :: forall st action slots msg m
   . MonadAff m
  => Row.Lacks "visibility" st
  => Row.Lacks "modalSubscriptionId" st
  => (action -> HalogenM st action slots msg m Unit)
  -> (Event -> HalogenM st action slots msg m Unit)
  -> Action action
  -> HalogenM st action slots msg m Unit
handleAction handleAction' handleEvent = case _ of
  Initialize mbAction -> do
    modalSid <- initializeWith DismissModal
    H.modify_ _ { modalSubscriptionId = Just modalSid }
    for_ mbAction handleAction'

  Finalize mbAction -> do
    state <- H.get
    for_ (state.modalSubscriptionId) \sid -> do
      close sid
      H.modify_ _ { modalSubscriptionId = Nothing }
    for_ mbAction handleAction'

  DismissModal -> do
    H.modify_ _ { visibility = Closed }
    handleEvent ModalDismissed
    
  OpenModal -> do
    H.modify_ _ { visibility = Open }
    pure unit

  Action action -> handleAction' action
