module Lily.Storybook.Main where

import Prelude

import Data.Maybe (Maybe(..))
import Data.Tuple (Tuple(Tuple))
import Data.Tuple.Nested ((/\))
import Data.Foldable (for_)

import Effect (Effect)
import Effect.Aff.Class (class MonadAff)

import Foreign.Object as Object

import Web.DOM.ParentNode (QuerySelector(..))

import Halogen.HTML as HH
import Halogen.Aff.Util (selectElement)
import Halogen.Aff as HA
import Halogen.Storybook (Stories, runStorybook, proxy)

import Lily.Storybook.Example.Modal as ExampleModal
import Lily.Storybook.Example.ModalRenderless as ExampleModalRenderless
import Lily.Storybook.Example.ModalHook as ModalHook


stories :: forall m.  MonadAff m => Stories m
stories = Object.fromFoldable
  [ Tuple "Parts|Modal" $ proxy ExampleModal.component
  , "Hooks|Modal (Beta)" /\ ModalHook.component
  , Tuple "Renderless|Modal" $ proxy ExampleModalRenderless.component ]


main :: Effect Unit
main = HA.runHalogenAff do
  body <- HA.awaitBody
  maybeContainer <- selectElement (QuerySelector "#storybook")
  for_ (maybeContainer) \element -> do
    runStorybook
      { stories
      , logo: Just $ HH.text "Lily Storybook"
      }
      element
