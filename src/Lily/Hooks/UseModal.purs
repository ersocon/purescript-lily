module Lily.Hooks.UseModal
  ( useModal
  , UseModal
  )
  where

import Prelude

import Data.Foldable (for_)


import Data.Maybe (Maybe(..))
import Data.Newtype (class Newtype)
import Data.Tuple.Nested ((/\))
import Effect.Aff.Class (class MonadAff)
import Effect.Class (liftEffect)
import Halogen as H
import Halogen.Hooks (Hook, HookM, UseEffect, UseState)
import Halogen.Hooks as Hooks
import Halogen.Query.EventSource as ES
import Halogen.Aff.Util (selectElement)
import Unsafe.Coerce (unsafeCoerce)
import Web.DOM.ParentNode (QuerySelector(..))
import Web.DOM.Element (setAttribute, removeAttribute)

import Web.Event.Event (EventType(..))
import Web.Event.Event as Event
import Web.Event.EventTarget (EventTarget)
import Web.HTML as HTML
import Web.HTML.Window as Window
import Web.HTML.HTMLElement (toElement)


import Web.UIEvent.KeyboardEvent as KE
import Web.UIEvent.KeyboardEvent.EventTypes as KET

newtype UseModal hooks = UseModal (UseEffect hooks)


derive instance newtypeUseModal :: Newtype (UseModal hooks) _

useModal :: forall m. MonadAff m => HookM m Unit -> Hook m UseModal Unit
useModal fn = Hooks.wrap Hooks.do
  Hooks.useLifecycleEffect do
    maybeHtml <- H.liftAff $ selectElement (QuerySelector "body")
    for_ (maybeHtml) \element ->
      H.liftEffect $ setAttribute "data-modal-mode" "true" (toElement element)
      
    subscription <- subscribeToEsc
    pure $ Just $ do
      maybeHtml <- H.liftAff $ selectElement (QuerySelector "body")
      for_ (maybeHtml) \element ->
        H.liftEffect $ removeAttribute "data-modal-mode" (toElement element)
      
      Hooks.unsubscribe subscription

  Hooks.pure unit
  where
    subscribeToEsc :: HookM m H.SubscriptionId
    subscribeToEsc = do
      window <- liftEffect HTML.window
      subscriptionId <- Hooks.subscribe do
        ES.eventListenerEventSource
          KET.keyup
          (Window.toEventTarget window)
          (handleKey <=< KE.fromEvent)
      pure subscriptionId
    
    --handleKey :: KE.KeyboardEvent -> Maybe (HookM m Unit)
    handleKey keyboardEvent = case (KE.key keyboardEvent) of
      "Escape" -> Just fn
      _ -> Nothing

    --handleKey keyboardEvent = Just $ Hooks.do fn

-- This function is missing from the purescript-web-html repository
fromEventTarget :: EventTarget -> HTML.Window
fromEventTarget = unsafeCoerce
