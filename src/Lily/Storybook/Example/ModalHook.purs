module Lily.Storybook.Example.ModalHook where

import Prelude

import Data.Maybe (Maybe(..), maybe)
import Data.Tuple.Nested ((/\))
import Data.Symbol (SProxy(..))
import Effect.Aff.Class (class MonadAff)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.Hooks as Hooks

import Lily.Hooks.UseModal (useModal)
import Lily.Block.Conditional (conditional_)

_innerModal :: SProxy "modal"
_innerModal = SProxy

component :: forall q i o m. MonadAff m => H.Component HH.HTML q i o m
component = Hooks.component \_ _ -> Hooks.do
  modalOpen /\ modalOpenState <- Hooks.useState false

  let
    handleModalMessage (ModalCloseRequested) = Just do
      Hooks.modify_ modalOpenState (\_ -> false)

    handleClick = Just do
      Hooks.modify_ modalOpenState (\_ -> true)
  Hooks.pure do
    HH.div_
      [ HH.text $ "This is an example of a modal"
      , HH.button
          [ HE.onClick \_ -> handleClick  ]
          [ HH.text "Click to open modal" ]
      , conditional_ 
          (modalOpen)
          [ HH.slot _innerModal unit modal "This is some input text for the modal" handleModalMessage ]
      ]


type ModalInput = String

data ModalMessage = ModalCloseRequested

modal :: forall q m. MonadAff m => H.Component HH.HTML q ModalInput ModalMessage m
modal = Hooks.component \{ outputToken } inputText -> Hooks.do
  text /\ textState <- Hooks.useState $ Just ""

  useModal (Hooks.raise outputToken ModalCloseRequested)

  let
    handleClick = Just do
      Hooks.raise outputToken ModalCloseRequested
    
  Hooks.pure do
    HH.div_
      [ HH.p_
          [ HH.text inputText ]
      , HH.text "Press ESC to close the modal. "
      , HH.text $ "Modal internals: " <> maybe "Modal Open" show text
      , HH.button
          [ HE.onClick $ \_ -> handleClick ]
          [ HH.text "Click to close modal from inside" ]
      ]
