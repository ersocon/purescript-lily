module Lily.Storybook.Example.ModalRenderless where

import Prelude

import Effect.Aff.Class (class MonadAff)

import Data.Maybe (Maybe(..))
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE

import Lily.Component.Renderless.Modal as M
import Lily.Component.Renderless.Modal.Setters as MS
import Lily.Block.Button as BTN
import Lily.Block.Conditional (conditional_)



type Slot =
  H.Slot M.Query' Message

type State =
  (  )

data Message
  = Void

data Action = Initialize

-- it is unnecessary to export your own input type, but doing so helps if you
-- would like to set some sensible defaults behind the scenes.
type Input = Unit

component :: forall m. MonadAff m => H.Component HH.HTML M.Query' Input Message m
component = M.component input $ M.defaultSpec
  { render = render
--  , handleEvent = handleEvent
  , handleAction = handleAction
  , initialize = Just Initialize
  }
  where
  input :: forall i . i -> M.Input State
  input _ = {}

--  handleEvent :: M.Event -> H.HalogenM (M.State State) (M.Action Action) () Message m Unit
--  handleEvent = case _ of
--    M.ModalDismissed -> do
--      st <- H.get
      --let selection = st.items !! ix
      --H.modify_ _ { selection = selection, visibility = S.Off }
      --H.raise $ SelectionChanged st.selection selection
--      pure unit

  handleAction :: Action -> H.HalogenM (M.State State) (M.Action Action) () Message m Unit
  handleAction action = case action of
    Initialize -> do
      H.modify_ _ { visibility = M.Open }
      pure unit


  render :: M.State State -> H.ComponentHTML (M.Action Action) () m
  render st =
    HH.div
      []
      [ conditional_ (st.visibility == M.Open)
          [ HH.text "Modal is now open"
          , BTN.buttonPrimary
              (MS.setDismissProps [])
              [ HH.text "Close Modal" ]
          ]
      , conditional_ (st.visibility == M.Closed)
          [ HH.text "Modal is now closed" ]
      , BTN.buttonPrimary
          [ HE.onClick \_ -> Just $ M.OpenModal ]
          [ HH.text "Toggle Modal" ]
      ]
